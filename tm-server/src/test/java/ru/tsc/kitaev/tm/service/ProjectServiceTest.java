package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;

public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProjectDescription";

    @NotNull
    private final String userId;

    public ProjectServiceTest() {
        userId = userService.create("test", "test").getId();
        project = projectService.create(userId, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        @NotNull final ProjectDTO newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(projectName, projectService.findByName(userId, projectName).getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        @NotNull final ProjectDTO newProject = projectService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIndexTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateByIndex(userId, 0,newProjectName, newProjectDescription);
        @NotNull final ProjectDTO newProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.removeById(userId, projectId);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectService.removeByIndex(userId, 0);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.removeByName(userId, projectName);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() {
        projectService.clear(userId);
        userService.removeById(userId);
    }

}
