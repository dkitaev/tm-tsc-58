package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private final TaskDTO task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTaskDescription";

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    public TaskRepositoryTest() {
        user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
        task = new TaskDTO();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        projectRepository.add(project);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(userId, 0));
        Assert.assertEquals(task, taskRepository.findByName(userId, taskName));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.removeById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIndexTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.removeByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void removeTaskByNameTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.removeByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.findById(userId, taskId).setProjectId(projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task, taskRepository.findAllTaskByProjectId(userId, projectId).get(0));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.findById(userId, taskId).setProjectId(projectId);
        entityManager.getTransaction().commit();
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setName("testTask1");
        task1.setUserId(userId);
        taskRepository.add(task1);
        entityManager.getTransaction().begin();
        taskRepository.findById(userId, task1.getId()).setProjectId(projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        entityManager.getTransaction().begin();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        taskRepository.clearByUserId(userId);
        projectRepository.clearByUserId(userId);
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
