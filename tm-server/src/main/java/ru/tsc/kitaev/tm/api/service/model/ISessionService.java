package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

public interface ISessionService {

    @NotNull
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password);

    @NotNull
    Session sign(@NotNull Session session);

    void close(@Nullable Session session);

}
