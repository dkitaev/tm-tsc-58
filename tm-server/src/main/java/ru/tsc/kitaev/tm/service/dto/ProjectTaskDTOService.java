package ru.tsc.kitaev.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.api.service.ILoggerService;
import ru.tsc.kitaev.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskDTOService extends AbstractDTOService implements IProjectTaskDTOService {

    @NotNull
    public IProjectDTORepository getProjectRepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @NotNull
    public ITaskDTORepository getTaskRepository() {
        return context.getBean(ITaskDTORepository.class);
    }

    @NotNull
    @Autowired
    private ILoggerService logService;

    @NotNull
    @Override
    public List<TaskDTO> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void bindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            @NotNull final String projectId = projectRepository.findByIndex(userId, index).getId();
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            @NotNull final String projectId = projectRepository.findByName(userId, name).getId();
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
