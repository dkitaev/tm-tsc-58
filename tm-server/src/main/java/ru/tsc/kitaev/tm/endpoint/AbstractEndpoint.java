package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.service.dto.ISessionDTOService;

@Controller
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected ISessionDTOService sessionService;

}
