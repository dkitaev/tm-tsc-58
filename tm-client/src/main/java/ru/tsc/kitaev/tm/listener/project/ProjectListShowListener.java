package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.ProjectDTO;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListShowListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list...";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable final List<ProjectDTO> projects;
        System.out.println("[SHOW PROJECTS]");
        if (sort.isEmpty()) projects = projectEndpoint.findProjectAll(sessionService.getSession());
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectEndpoint.findProjectAllSorted(sessionService.getSession(), sort);
        }
        for (@NotNull final ProjectDTO project: projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " +project.getName() + ". " + project.getStatus());
        }
        System.out.println("[OK]");
    }

}
