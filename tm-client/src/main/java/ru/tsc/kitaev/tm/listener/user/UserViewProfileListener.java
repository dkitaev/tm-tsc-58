package ru.tsc.kitaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;
import ru.tsc.kitaev.tm.endpoint.UserDTO;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;

@Component
public class UserViewProfileListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "view-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile...";
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final UserDTO user = userEndpoint.findById(sessionService.getSession());
        if (user == null) throw new EmptyUserIdException();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E_MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

}
