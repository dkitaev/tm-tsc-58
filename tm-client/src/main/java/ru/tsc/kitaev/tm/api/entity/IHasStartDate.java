package ru.tsc.kitaev.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasStartDate {

    @Nullable Date getStartDate();

    void setStartDate(@NotNull Date startDate);

}
