package ru.tsc.kitaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public class UserByLoginRemoveListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "user-remove-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    @EventListener(condition = "@userByLoginRemoveListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.removeByLogin(sessionService.getSession(), login);
        System.out.println("[OK]");
    }

}
